module gitee.com/chenke1115/team

go 1.12

require (
	github.com/GeertJohan/go.rice v1.0.3
	github.com/go-sql-driver/mysql v1.5.0
	gopkg.in/ldap.v3 v3.1.0
)
